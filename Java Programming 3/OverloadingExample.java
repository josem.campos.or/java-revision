public class OverloadingExample {
    public static void printMessage() {
        System.out.println("Hello this is a default message");
    }
    public static void printMessage(String name) {
        System.out.println("Hello " + name + " this is a default message ");
    }
    public static void printMessage(String name,int age) {
        System.out.println("Hello " + name + " this is a default message and your age is "+ age);
    }

    public static void main(String[] args) {
        OverloadingExample.printMessage();
        OverloadingExample.printMessage("Pedro");
        OverloadingExample.printMessage("Luis", 15);


    }
}
