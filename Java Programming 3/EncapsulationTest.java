public class EncapsulationTest {
    public static void main(String[] args)
    {
         Encapsulation obj = new Encapsulation();
         //due to obj.userName; is not validated (becaouse is a private atribute, we must use a public getter or setter function
         obj.setUserName("Pedro Paramo");
         obj.setUserAge(22);
         obj.setUserID(12134);
         System.out.println("User's name: " + obj.getUserName());
         System.out.println("User's age: " + obj.getUserAge());
         System.out.println("User's id: " + obj.getUserID());
    }
}
