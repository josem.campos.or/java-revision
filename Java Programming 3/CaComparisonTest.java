public class CaComparisonTest {
    public static void main(String[] args) {
        Car Xtrail1 = new Car("Xtrail",300000,2019);
        Car Xtrail2 = new Car("Xtrail",500000,2022);
        Car Xtrail3 = new Car("Xtrail",300000,2019);
        Car Lobo = new Car("Lobo",900000,2020);
        System.out.println("Xtrail1 is equal to Xtrail2? "+ Xtrail1.equals(Xtrail2));
        System.out.println("Xtrail1 is equal to Xtrail3? "+ Xtrail1.equals(Xtrail3));
        System.out.println("Xtrail1 is equal to Lobo? "+ Xtrail1.equals(Lobo));

    }
}
