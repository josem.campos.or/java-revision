public class PolymorphismTest {
    public static void main(String[] args) {
        Animal dog = new Dog();
        Animal bird = new Bird();
        dog.eat();
        dog.move();
        bird.eat();
        bird.move();
    }
}
