public class Dog extends Animal {
    void eat(){
        System.out.println("The dog eats kibble");
    }
    void move(){
        System.out.println("The dog moves with 4 legs");
    }
}
