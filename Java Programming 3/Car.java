public class Car {
    String model;
    int price;
    int sellingYear;
    public Car(String model,int price,int sellingYear){
        this.model = model;
        this.price = price;
        this.sellingYear = sellingYear;
    }
    @Override public boolean equals(Object obj)
    {
        if (this == obj){
            return true;
        }

        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        Car c1 = (Car)obj;

        return this.model.equals(c1.model)
                && this.price == c1.price
                && this.sellingYear == c1.sellingYear ;
    }
}
