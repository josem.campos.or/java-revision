public class Third {
    public static void main(String[] args) {
        printFibonacciNum(25);
    }
    public static void printFibonacciNum(long lim){
        long aux = 1;//y
        long fib = 1;//z
        long init = 0;//x
        if(lim > 0) {
            for(int i = 1; i <= lim; i++) {
                fib = init + aux;
                System.out.println(" " + fib + " ");
                init = aux;
                aux = fib;

            }
        } else {
            System.out.println(" Limit must be greater than 0 ");

        }
        System.out.println(" \n");
    }
}
