public class fibbonaci {
    public static void main(String[] args) {
        print_serie(5);
    }
    public static int  fibbonaci_recursive(int n){
        if (n == 0){
            return 0;
        }
        else if (n == 1){
            return 1;
        }
        else{
        return fibbonaci_recursive(n-1) + fibbonaci_recursive(n-2) ;
        }
    }

    public static void print_serie(int n ){
        int maxCount = n;
        for (int i = 2; i <= maxCount + 1; i++) {
            int fibbonacciNumber = fibbonaci_recursive(i);
            System.out.print(" " + fibbonacciNumber);
        }
    }
}
